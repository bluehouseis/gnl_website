import Phone from "../../images/footer/phone.svg";
import Home from "../../images/footer/home.svg";
import Mail from "../../images/footer/mail.svg";
import Map from "../../images/footer/map.svg";
import Whatsapp from "../../images/footer/whatsapp.svg";

export const LinksInfo = [
  {
    target: "tel:+3547756480",
    icon: Phone,
    info: "+354 775 6480",
  },
  {
    target: "mailto:info@grottanorthernlights.com",
    icon: Mail,
    info: "info@grottanorthernlights.com",
  },
  {
    target: "https://www.google.com/maps/place/Gr%C3%B3tta+Northern+Lights+Apartment+%26+Rooms/@64.1540629,-22.0161935,14z/data=!4m16!1m6!2m5!1sgrotta+northern+lights!5m3!5m2!4m1!1i2!3m8!1s0x48d60bc5620ec2cd:0xefef4fc51c050a2a!5m2!4m1!1i2!8m2!3d64.1558286!4d-21.9969558!15sChZncm90dGEgbm9ydGhlcm4gbGlnaHRzkgEbc2VsZl9jYXRlcmluZ19hY2NvbW1vZGF0aW9u",
    icon: Map,
    info: "170 Setjarnarnes, Iceland",
  },
  {
    target: "https://www.bluehouse.is/",
    icon: Home,
    info: "Bluehouse",
    info2: "B&B Apartment",
  },
  {
    target:
      "https://api.whatsapp.com/send?phone=3547756480&text=&source=&data=",
    icon: Whatsapp,
    info: "Whatsapp",
  },
];
export const LinksInfo2 = [
  {
    target: "/house-rules",
    info: "House Rules",
  },
  {
    target: "https://blog.bluehouse.is/",
    info: "Blue House Blog",
  },
  {
    target:
      "https://bluehouse.tourdesk.is/Tour/Item/18676/1/Northern_lights_-_Minibus",
    info: "Northern Lights",
  },
  {
    target: "https://gnl.ladesk.com/219394-Feedback",
    info: " Feedback",
  },
  {
    target: "/privacy-and-policy",
    info: " Imprint & Privacy Policy",
  },
];
