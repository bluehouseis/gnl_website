import triple1 from "../../images/Rooms/TripleQuadruple/t1.jpg";
import triple2 from "../../images/Rooms/TripleQuadruple/t2.jpg";
import triple3 from "../../images/Rooms/TripleQuadruple/t3.jpg";
import triple4 from "../../images/Rooms/TripleQuadruple/t4.jpg";
import triple5 from "../../images/Rooms/TripleQuadruple/t5.jpg";
import triple6 from "../../images/Rooms/TripleQuadruple/t6.jpg";
import triple7 from "../../images/Rooms/TripleQuadruple/t7.jpg";
import triple8 from "../../images/Rooms/TripleQuadruple/t8.jpg";
import triple9 from "../../images/Rooms/TripleQuadruple/t9.jpg";
import triple10 from "../../images/Rooms/TripleQuadruple/t10.jpg";

import family1 from "../../images/Rooms/FamilyRoom/fr1.jpg";
import family2 from "../../images/Rooms/FamilyRoom/fr2.jpg";
import family3 from "../../images/Rooms/FamilyRoom/fr3.jpg";
import family4 from "../../images/Rooms/FamilyRoom/fr4.jpg";
import family5 from "../../images/Rooms/FamilyRoom/fr5.jpg";
import family6 from "../../images/Rooms/FamilyRoom/fr6.jpg";

import apartments1 from "../../images/Rooms/Apartment/a1.jpg";
import apartments2 from "../../images/Rooms/Apartment/a2.jpg";
import apartments3 from "../../images/Rooms/Apartment/a3.jpg";
import apartments4 from "../../images/Rooms/Apartment/a4.jpg";
import apartments5 from "../../images/Rooms/Apartment/a5.jpg";
import apartments6 from "../../images/Rooms/Apartment/a6.jpg";
import apartments7 from "../../images/Rooms/Apartment/a7.jpg";
import apartments8 from "../../images/Rooms/Apartment/a8.jpg";
import apartments9 from "../../images/Rooms/Apartment/a9.jpg";
import apartments10 from "../../images/Rooms/Apartment/a10.jpg";
import apartments11 from "../../images/Rooms/Apartment/a11.jpg";
import apartments12 from "../../images/Rooms/Apartment/a12.jpg";
import apartments13 from "../../images/Rooms/Apartment/a13.jpg";
import apartments14 from "../../images/Rooms/Apartment/a14.jpg";
import apartments15 from "../../images/Rooms/Apartment/a15.jpg";
import apartments16 from "../../images/Rooms/Apartment/a16.jpg";
import apartments17 from "../../images/Rooms/Apartment/a17.jpg";
import apartments18 from "../../images/Rooms/Apartment/a18.jpg";
import apartments19 from "../../images/Rooms/Apartment/a19.jpg";
import apartments20 from "../../images/Rooms/Apartment/a20.jpg";
import apartments21 from "../../images/Rooms/Apartment/a21.jpg";
import apartments22 from "../../images/Rooms/Apartment/a22.jpg";
import apartments23 from "../../images/Rooms/Apartment/a23.jpg";
import apartments24 from "../../images/Rooms/Apartment/a24.jpg";
import apartments25 from "../../images/Rooms/Apartment/a25.jpg";

import neighborhoodPic1 from "../../images/view-gallery/neighborhood.jpeg";
import neighborhoodPic2 from "../../images/view-gallery/neighborhood2.jpg";
import neighborhoodPic3 from "../../images/view-gallery/neighborhood3.jpg";

import northernLightsPic3 from "../../images/Surroundings/surrounding3.jpg";
import NorthernLightsPic1 from "../../images/view-gallery/northern_lights.jpeg";
import northernLightsPic2 from "../../images/view-gallery/northern_lights3.jpg";

import activitiesPic from "../../images/view-gallery/Activities/1.jpg";
import activitiesPic2 from "../../images/view-gallery/Activities/2.jpg";
import activitiesPic3 from "../../images/view-gallery/Activities/3.jpg";
import Activites4 from "../../images/view-gallery/Activities/4.jpg";


export const tripleRoom = [
  {
    id: 1,
    title: "Triple / Quadruple",
    background: triple1,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 2,
    title: "Triple / Quadruple",
    background: triple2,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 3,
    title: "Triple / Quadruple",
    background: triple3,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 4,
    title: "Triple / Quadruple",
    background: triple4,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 5,
    title: "Triple / Quadruple",
    background: triple5,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 6,
    title: "Triple / Quadruple",
    background: triple6,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 7,
    title: "Triple / Quadruple",
    background: triple7,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 8,
    title: "Triple / Quadruple",
    background: triple8,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 9,
    title: "Triple / Quadruple",
    background: triple9,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
  {
    id: 10,
    title: "Triple / Quadruple",
    background: triple10,
    description:
      "The Triple/Quadruple Rooms are located in two of the three buildings. They feature a sofa bed and flat screen TVs with Netflix. Twin beds can be requested depending on availability. Bathroom and kitchen facilities are shared.",
  },
];
export const familyRoom = [
  {
    id: 1,
    title: "Family Room",
    background: family1,
    description:
      "The Family Room with a terrace overlooking the ocean and the lighthouse is located in the Grótta Northern Lights Apartment in Valhúsabraut 35. It includes a TV with Netflix, as well as a private bathroom and dining area. Kitchen facilities are shared.",
  },
  {
    id: 2,
    title: "Family Room",
    background: family2,
    description:
      "The Family Room with a terrace overlooking the ocean and the lighthouse is located in the Grótta Northern Lights Apartment in Valhúsabraut 35. It includes a TV with Netflix, as well as a private bathroom and dining area. Kitchen facilities are shared.",
  },
  {
    id: 3,
    title: "Family Room",
    background: family3,
    description:
      "The Family Room with a terrace overlooking the ocean and the lighthouse is located in the Grótta Northern Lights Apartment in Valhúsabraut 35. It includes a TV with Netflix, as well as a private bathroom and dining area. Kitchen facilities are shared.",
  },
  {
    id: 4,
    title: "Family Room",
    background: family4,
    description:
      "The Family Room with a terrace overlooking the ocean and the lighthouse is located in the Grótta Northern Lights Apartment in Valhúsabraut 35. It includes a TV with Netflix, as well as a private bathroom and dining area. Kitchen facilities are shared.",
  },
  {
    id: 5,
    title: "Family Room",
    background: family5,
    description:
      "The Family Room with a terrace overlooking the ocean and the lighthouse is located in the Grótta Northern Lights Apartment in Valhúsabraut 35. It includes a TV with Netflix, as well as a private bathroom and dining area. Kitchen facilities are shared.",
  },
  {
    id: 6,
    title: "Family Room",
    background: family6,
    description:
      "The Family Room with a terrace overlooking the ocean and the lighthouse is located in the Grótta Northern Lights Apartment in Valhúsabraut 35. It includes a TV with Netflix, as well as a private bathroom and dining area. Kitchen facilities are shared.",
  },
];
export const apartmentRoom = [
  // {
  //   id: 1,
  //   title: "Apartments",
  //   background: apartments,
  //   description:
  //     "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  // },
  {
    id: 1,
    title: "Apartments",
    background: apartments1,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 2,
    title: "Apartments",
    background: apartments2,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 3,
    title: "Apartments",
    background: apartments3,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 4,
    title: "Apartments",
    background: apartments4,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 5,
    title: "Apartments",
    background: apartments5,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 6,
    title: "Apartments",
    background: apartments6,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 7,
    title: "Apartments",
    background: apartments7,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 8,
    title: "Apartments",
    background: apartments8,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 9,
    title: "Apartments",
    background: apartments9,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 10,
    title: "Apartments",
    background: apartments10,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 11,
    title: "Apartments",
    background: apartments11,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 12,
    title: "Apartments",
    background: apartments12,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 13,
    title: "Apartments",
    background: apartments13,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 14,
    title: "Apartments",
    background: apartments14,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 15,
    title: "Apartments",
    background: apartments15,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 16,
    title: "Apartments",
    background: apartments16,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 17,
    title: "Apartments",
    background: apartments17,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 18,
    title: "Apartments",
    background: apartments18,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 19,
    title: "Apartments",
    background: apartments19,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 20,
    title: "Apartments",
    background: apartments20,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 21,
    title: "Apartments",
    background: apartments21,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 22,
    title: "Apartments",
    background: apartments22,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 23,
    title: "Apartments",
    background: apartments23,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 24,
    title: "Apartments",
    background: apartments24,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
  {
    id: 25,
    title: "Apartments",
    background: apartments25,
    description:
      "Our two-bedroom apartment is located in the garden of the Blue House, while our three-bedroom apartment with sea view is located 150 metres from it. Both are equipped with a kitchenette, private bathrooms, sofa beds and Netflix TVs.",
  },
];

export const northernLights = [
  {
    id: 1,
    title: "Northern Lights",
    background: NorthernLightsPic1,
    description:
      "Close to the Blue House, the best place to spot northern lights is by Grótta Lighthouse. Located on the edge of Seltjarnarnes Peninsula in a protected nature reserve home to more than 100 different bird species and 140 plant varieties, the lighthouse was built in 1897 and fully restored in 1947. You can easily reach it with a pleasant 25-minutes walk from the Blue House.",
  },
  {
    id: 2,
    title: "Northern Lights",
    background: northernLightsPic2,
    description:
      "Close to the Blue House, the best place to spot northern lights is by Grótta Lighthouse. Located on the edge of Seltjarnarnes Peninsula in a protected nature reserve home to more than 100 different bird species and 140 plant varieties, the lighthouse was built in 1897 and fully restored in 1947. You can easily reach it with a pleasant 25-minutes walk from the Blue House.",
  },
  {
    id: 3,
    title: "Northern Lights",
    background: northernLightsPic3,
    description:
      "Close to the Blue House, the best place to spot northern lights is by Grótta Lighthouse. Located on the edge of Seltjarnarnes Peninsula in a protected nature reserve home to more than 100 different bird species and 140 plant varieties, the lighthouse was built in 1897 and fully restored in 1947. You can easily reach it with a pleasant 25-minutes walk from the Blue House.",
  },
];
export const neighborhood = [
  {
    id: 1,
    title: "Neighborhood",
    background: neighborhoodPic1,
    // "https://i2.wp.com/bluehouse.is/wp-content/uploads/2020/04/rsz_1contactus-e1608110467521.jpg?fit=1000%2C563&ssl=1",
    description:
      "Seltjarnarnes is a very quiet town: you will definitely have a very relaxing stay with us! You will often see kids playing or running in the streets, parents walking with their toddlers, and cute, furry cats strolling around undisturbed. Did you know that in 2007 Seltjarnarnes became the world's first town where every citizen had access to fiber optics?",
  },
  {
    id: 2,
    title: "Neighborhood",
    background: neighborhoodPic2,
    description:
      "Seltjarnarnes is a very quiet town: you will definitely have a very relaxing stay with us! You will often see kids playing or running in the streets, parents walking with their toddlers, and cute, furry cats strolling around undisturbed. Did you know that in 2007 Seltjarnarnes became the world's first town where every citizen had access to fiber optics?",
  },
  {
    id: 3,
    title: "Neighborhood",
    background: neighborhoodPic3,
    description:
      "Seltjarnarnes is a very quiet town: you will definitely have a very relaxing stay with us! You will often see kids playing or running in the streets, parents walking with their toddlers, and cute, furry cats strolling around undisturbed. Did you know that in 2007 Seltjarnarnes became the world's first town where every citizen had access to fiber optics?",
  },
];
export const activities = [
  {
    id: 1,
    title: "Activities",
    background: Activites4,
    description:
      "While visiting Seltjarnarnes Peninsula, stroll through Valhúsahæð Park, behind the Blue House, then head to the Grótta Lighthouse, located in a protected nature reserve. Don’t miss the Kvika Foot Bath, a geothermal small pool that allows you to enjoy the beautiful view of Esja Mountain and Snæfellsjökull Glacier.",
  },
  {
    id: 2,
    title: "Activities",
    background: activitiesPic,
    description:
      "While visiting Seltjarnarnes Peninsula, stroll through Valhúsahæð Park, behind the Blue House, then head to the Grótta Lighthouse, located in a protected nature reserve. Don’t miss the Kvika Foot Bath, a geothermal small pool that allows you to enjoy the beautiful view of Esja Mountain and Snæfellsjökull Glacier.",
  },
  {
    id: 3,
    title: "Activities",
    background: activitiesPic2,
    description:
      "While visiting Seltjarnarnes Peninsula, stroll through Valhúsahæð Park, behind the Blue House, then head to the Grótta Lighthouse, located in a protected nature reserve. Don’t miss the Kvika Foot Bath, a geothermal small pool that allows you to enjoy the beautiful view of Esja Mountain and Snæfellsjökull Glacier.",
  },
  {
    id: 4,
    title: "Activities",
    background: activitiesPic3,
    description:
      "While visiting Seltjarnarnes Peninsula, stroll through Valhúsahæð Park, behind the Blue House, then head to the Grótta Lighthouse, located in a protected nature reserve. Don’t miss the Kvika Foot Bath, a geothermal small pool that allows you to enjoy the beautiful view of Esja Mountain and Snæfellsjökull Glacier.",
  },
];

export const sliderData_Rooms = [
  {
    title: "Triple/Quadruple",
    imagesAndDescription: tripleRoom,
  },
  {
    title: "Family Room",
    imagesAndDescription: familyRoom,
  },
  {
    title: "Three-bedroom Apartment",
    imagesAndDescription: apartmentRoom,
  },
];


export const sliderData_Surroundings = [
  {
    title: "Northern Lights",
    imagesAndDescription: northernLights,
  },
  {
    title: "Neighborhood",
    imagesAndDescription: neighborhood,
  },
  {
    title: "Activities",
    imagesAndDescription: activities,
  },
];

const sliderData = [
  sliderData_Rooms,
  sliderData_Surroundings,
];
export { sliderData };
